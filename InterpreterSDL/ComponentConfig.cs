using System;
using System.Runtime.InteropServices;
using GME.Util;
using GME.MGA;

namespace GME.CSharp
{
    
    abstract class ComponentConfig
    {
        // Set paradigm name. Provide * if you want to register it for all paradigms.
		public const string paradigmName = "SDL";
		
		// Set the human readable name of the interpreter. You can use white space characters.
        public const string componentName = "InterpreterSDL";
        
		// Specify an icon path
		public const string iconName = "Component.ico";
        
		public const string tooltip = "InterpreterSDL";

		// If null, updated with the assembly path + the iconName dynamically on registration
        public static string iconPath = null; 
        
		// Uncomment the flag if your component is paradigm independent.
		public static componenttype_enum componentType = componenttype_enum.COMPONENTTYPE_INTERPRETER;
				
        public const regaccessmode_enum registrationMode = regaccessmode_enum.REGACCESS_SYSTEM;
        public const string progID = "MGA.Interpreter.InterpreterSDL";
        public const string guid = "7177D1D2-3D66-4337-8C2A-F1905F2AC37A";
    }
}
