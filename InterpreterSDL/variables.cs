﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpreterSDL
{
    class variables
    {
        public static HashSet<string> ProcessTypes = new HashSet<string>()
            {
                "GenericProcess","OSProcess","ManagedApplication","WindowsStoreProcess","ThickClient","Win32Service","Thread",
                "BrowserClient","WebApplication","KernelThread","BrowserActiveX","WebService","NativeApplication","WebServer","VirtualMachine","NonMSOS"
            };
        public static HashSet<string> StoreTypes = new HashSet<string>()
            {"GenericDataStore","CloudStorage","ConfigurationFile","SQLDatabase","Cache","NonRelationalDatabase","HTML5LocalStorage","FileSystem","Cookies","Device"
            };

        public static HashSet<string> ExternalTypes = new HashSet<string>()
            {"Browser","HumanUser","AuthorizationProvider","MegaService","WindowsNETRuntime",
                "ExternalWebService","WindowsRuntime","WindowsRTRuntime","GenerricExtrenalInteractor","ExternalWebApplication"
            };
    }
}
