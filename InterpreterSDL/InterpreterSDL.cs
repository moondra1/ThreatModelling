using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using GME.CSharp;
using GME;
using GME.MGA;
using GME.MGA.Core;
using SDL = ISIS.GME.Dsml.SDL.Interfaces;
namespace InterpreterSDL
{
    /// <summary>
    /// This class implements the necessary COM interfaces for a GME interpreter component.
    /// </summary>
    [Guid(ComponentConfig.guid),
    ProgId(ComponentConfig.progID),
    ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    public class InterpreterSDLInterpreter : IMgaComponentEx, IGMEVersionInfo
    {
        /// <summary>
        /// Contains information about the GUI event that initiated the invocation.
        /// </summary>
        public enum ComponentStartMode
        {
            GME_MAIN_START = 0, 		// Not used by GME
            GME_BROWSER_START = 1,      // Right click in the GME Tree Browser window
            GME_CONTEXT_START = 2,		// Using the context menu by right clicking a model element in the GME modeling window
            GME_EMBEDDED_START = 3,		// Not used by GME
            GME_MENU_START = 16,		// Clicking on the toolbar icon, or using the main menu
            GME_BGCONTEXT_START = 18,	// Using the context menu by right clicking the background of the GME modeling window
            GME_ICON_START = 32,		// Not used by GME
            GME_SILENT_MODE = 128 		// Not used by GME, available to testers not using GME
        }

        /// <summary>
        /// This function is called for each interpreter invocation before Main.
        /// Don't perform MGA operations here unless you open a transaction.
        /// </summary>
        /// <param name="project">The handle of the project opened in GME, for which the interpreter was called.</param>
        public void Initialize(MgaProject project)
        {
            // TODO: Add your initialization code here...            
        }

        /// <summary>
        /// The main entry point of the interpreter. A transaction is already open,
        /// GMEConsole is available. A general try-catch block catches all the exceptions
        /// coming from this function, you don't need to add it. For more information, see InvokeEx.
        /// </summary>
        /// <param name="project">The handle of the project opened in GME, for which the interpreter was called.</param>
        /// <param name="currentobj">The model open in the active tab in GME. Its value is null if no model is open (no GME modeling windows open). </param>
        /// <param name="selectedobjs">
        /// A collection for the selected model elements. It is never null.
        /// If the interpreter is invoked by the context menu of the GME Tree Browser, then the selected items in the tree browser. Folders
        /// are never passed (they are not FCOs).
        /// If the interpreter is invoked by clicking on the toolbar icon or the context menu of the modeling window, then the selected items 
        /// in the active GME modeling window. If nothing is selected, the collection is empty (contains zero elements).
        /// </param>
        /// <param name="startMode">Contains information about the GUI event that initiated the invocation.</param>
        [ComVisible(false)]


        //StreamWriter sw = new  Stre(@"C:\Users\Arul\Desktop\sdl\Threats.txt", false);

        public void writereport(String Data)
        {
            String Current = Directory.GetCurrentDirectory();
            Current = Current + "\\Threats.txt";
            StreamWriter sw = File.AppendText(@Current);
            sw.WriteLine(Data);
            sw.Close();
            
        }

        public void checkandprintforpresent(String Attributename, IMgaModel Configurationfile, IMgaSimpleConnection flow)
        {
           
            {
                
                foreach (var SpecificConfiguration in Configurationfile.ChildFCOs.OfType<IMgaSimpleConnection>().Where(x => x.Src.Meta.Name.Equals(Attributename)))
                {
                    if ((SpecificConfiguration.get_BoolAttrByName("Presence")))
                    {
                        if (flow.get_BoolAttrByName("TrustThisFlow"))
                        {
                            if (SpecificConfiguration.get_BoolAttrByName("Trusttheflow"))

                                writereport("\t\t" +  SpecificConfiguration.Dst.get_StrAttrByName("Details"));
                        }
                        else
                        {
                            if (!(SpecificConfiguration.get_BoolAttrByName("Trusttheflow")))
                                writereport("\t\t" + SpecificConfiguration.Dst.get_StrAttrByName("Details"));
                        }
                    }

                }
            }
        }

        public void checkandprintfornotpresent(String Attributename, IMgaModel Configurationfile, IMgaSimpleConnection flow)
        {
          
            {
                int i = 0;
                foreach (var SpecificConfiguration in Configurationfile.ChildFCOs.OfType<IMgaSimpleConnection>().Where(x => x.Src.Meta.Name.Equals(Attributename)))
                {
                  //  i++;
                   // GMEConsole.Out.WriteLine(i+SpecificConfiguration.Meta.Name +SpecificConfiguration.Src.Meta.Name+ SpecificConfiguration.Dst.Meta.Name);
                    if (!(SpecificConfiguration.get_BoolAttrByName("Presence")))
                    {
                        if (flow.get_BoolAttrByName("TrustThisFlow"))
                        {
                            if (SpecificConfiguration.get_BoolAttrByName("Trusttheflow"))
                                writereport("\t\t "+SpecificConfiguration.Dst.Meta.Name+SpecificConfiguration.Dst.get_StrAttrByName("Details"));
                        }
                        else
                        {
                            if (!(SpecificConfiguration.get_BoolAttrByName("Trusttheflow")))
                               writereport("\t\t"+SpecificConfiguration.Dst.get_StrAttrByName("Details"));
                        }

                    }
                }
            }
        }

        public void processoftype(IMgaFolder rootFolder, IMgaFCO SpecificSource, IMgaSimpleConnection flow)
        {


            if (SpecificSource.get_BoolAttrByName("AuthenticationMechanism"))
            {
                foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                {
                    checkandprintforpresent("Process_AuthenticationMechanism", Configurationfile, flow);
                }
            }
            else
            {
                foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                {
                    checkandprintfornotpresent("Process_AuthenticationMechanism", Configurationfile, flow);
                }
            }

            if (SpecificSource.get_BoolAttrByName("AuthorizationMechanism"))
            {
                foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                {
                    checkandprintforpresent("Process_AuthorizationMechanism", Configurationfile, flow);
                }
            }
            else
            {
                foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                {
                    checkandprintfornotpresent("Process_AuthorizationMechanism", Configurationfile, flow);
                }
            }


            if (SpecificSource.get_BoolAttrByName("CommunicationProtocol"))
            {
                foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                {
                    checkandprintforpresent("Process_CommunicationProtocol", Configurationfile, flow);
                }
            }
            else
            {
                foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                {
                    checkandprintfornotpresent("Process_CommunicationProtocol", Configurationfile, flow);
                }
            }





        }

        public void Externaloftype(IMgaFolder rootFolder, IMgaFCO SpecificSource, IMgaSimpleConnection flow)
        {
                if (SpecificSource.get_BoolAttrByName("AuthenticatesItself"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("External_AuthenticatesItself", Configurationfile, flow);
                    }
                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("External_AuthenticatesItself", Configurationfile, flow);
                    }
                }

            }
        
        public void flowoftype(IMgaFolder rootFolder, IMgaSimpleConnection flow)
        {
           
                if (flow.get_BoolAttrByName("ProvidesIntegrity"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("Flow_ProvidesIntegrity", Configurationfile, flow);
                    }

                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("Flow_ProvidesIntegrity", Configurationfile, flow);
                    }
                }

                if (flow.get_BoolAttrByName("ProvidesConfidentiality"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("Flow_ProvidesConfidentiality", Configurationfile, flow);
                    }
                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("Flow_ProvidesConfidentiality", Configurationfile, flow);
                    }
                }

                if (flow.get_BoolAttrByName("SourceAuthenticated"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("Flow_Source_Authenticated", Configurationfile, flow);
                    }
                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("Flow_Source_Authenticated", Configurationfile, flow);
                    }
                }

                if (flow.get_BoolAttrByName("DestinationAuthenticated"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("Flow_Destination_Authenticated", Configurationfile, flow);
                    }
                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("Flow_Destination_Authenticated", Configurationfile, flow);
                    }
                }
            }
        
        public void Storeoftype(IMgaFolder rootFolder, IMgaFCO SpecificSource, IMgaSimpleConnection flow)
        {
            
                if (SpecificSource.get_BoolAttrByName("Encrypted"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("Store_encrypted", Configurationfile, flow);
                    }
                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("Store_encrypted", Configurationfile, flow);
                    }
                }


                if (SpecificSource.get_BoolAttrByName("Signed"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("Store_Signed", Configurationfile, flow);
                    }
                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("Store_Signed", Configurationfile, flow);
                    }
                }

                if (SpecificSource.get_BoolAttrByName("WriteAccess"))
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintforpresent("Store_writeAccess", Configurationfile, flow);
                    }
                }
                else
                {
                    foreach (var Configurationfile in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "ConfigureThreats"))
                    {
                        checkandprintfornotpresent("Store_writeAccess", Configurationfile, flow);
                    }
                }





            }
        
        public void flowsonfirstlevel(IMgaFolder rootFolder)
        {
            foreach (var currentDiagram in rootFolder.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "Diagram"))
            {
                GMEConsole.Out.WriteLine(currentDiagram.Name);
                writereport("Threats" + currentDiagram.Name);
                foreach (var outsideflow in currentDiagram.ChildFCOs.OfType<IMgaSimpleConnection>())
                {
                    var SpecificSource = outsideflow.Src;
                    flowoftype(rootFolder, outsideflow);
                }

                traversingthroughnext(currentDiagram,rootFolder);
            }
        }

        public void traversingthroughnext(IMgaModel currentDiagram , IMgaFolder rootFolder)
        {   
            foreach (var GenericModel in currentDiagram.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "GenericModel"))
                {
                    GMEConsole.Out.WriteLine(GenericModel.Name);
                    //using (sw)
                    {
                        writereport("Threats in" + GenericModel.Name);
                    }
                    //sw.WriteLine(Threat.SrcEnd.Name + "\t" + Threat.DstEnd.Name + "\t" + Threat.Attributes.Presence +"\t"+ Threat.Attributes.Trusttheflow);
                    foreach (var flows in GenericModel.ChildFCOs.OfType<IMgaSimpleConnection>())
                    {
                        // GMEConsole.Out.WriteLine(flows.Src.Meta.Name);
                        var SpecificSource = flows.Src;

                         
                        if (variables.ProcessTypes.Contains(flows.Src.Meta.Name))
                        {
                            //using (sw)
                            {
                                writereport("\tProcess " + flows.Src.Meta.Name);
                            }
                            flowoftype(rootFolder, flows);
                            processoftype(rootFolder, SpecificSource, flows);

                        }
                        else if (variables.StoreTypes.Contains(flows.Src.Meta.Name))
                        {
                            // using (sw)
                            {
                                writereport("\tStore " + flows.Src.Meta.Name);
                            }
                            flowoftype(rootFolder, flows);
                            Storeoftype(rootFolder, SpecificSource, flows);
                        }

                        else if (variables.ExternalTypes.Contains(flows.Src.Meta.Name))
                        {
                            //using (sw)
                            {
                                writereport("\tExternal " + flows.Src.Meta.Name);
                            }
                            flowoftype(rootFolder, flows);
                            Externaloftype(rootFolder, SpecificSource, flows);
                        }




                    } if ((GenericModel.ChildFCOs.OfType<IMgaModel>().Count<IMgaModel>() != 0))//Count<IMgaModel>() != 0))
                    {
                        foreach (var GenericModel1 in currentDiagram.ChildFCOs.OfType<IMgaModel>().Where(x => x.Meta.Name == "GenericModel"))
                            traversingthroughnext(GenericModel1, rootFolder);
                    }
                }
        }

        public void Main(MgaProject project, MgaFCO currentobj, MgaFCOs selectedobjs, ComponentStartMode startMode)
        {
            String Current = Directory.GetCurrentDirectory();
            Current = Current + "\\Threats.txt";
            File.Delete(@Current);

            
           // StreamWriter sw = File.AppendText(@"C:\Users\Arul\Desktop\sdl\Threats.txt");
           
           
                // TODO: Add your interpreter code
                GMEConsole.Out.WriteLine("Running interpreter...");

                // Get RootFolder
                IMgaFolder rootFolder = project.RootFolder;
                //GMEConsole.Out.WriteLine(rootFolder.Name);



                var rootfolder = ISIS.GME.Dsml.SDL.Classes.RootFolder.GetRootFolder(project);
                var configuration = rootfolder.Children.ConfigureThreatsCollection;

                GMEConsole.Out.WriteLine(rootfolder.Name);
                var tempfilepath = Path.GetTempFileName();

                flowsonfirstlevel(rootFolder);


                
          
            System.Diagnostics.Process.Start("notepad.exe", @Current);

            /* var tempfilepath = Path.GetTempFileName();
             using (var sw = new StreamWriter(@"C:\Users\Arul\Desktop\sdl\ConfigurationofThreats.txt", false))
             {
                 foreach (var currentmodel in rootfolder.Children.ConfigureThreatsCollection)
                 {
                     sw.WriteLine(currentmodel.Name);
                     GMEConsole.Out.WriteLine(currentmodel.Name);
                     foreach (var Threat in currentmodel.Children.ThreatsCollection)
                     {
                         sw.WriteLine(Threat.SrcEnd.Name + "\t" + Threat.DstEnd.Name + "\t" + Threat.Attributes.Presence +"\t"+ Threat.Attributes.Trusttheflow);
                     }
                 }
             }
             System.Diagnostics.Process.Start("notepad.exe", @"C:\Users\Arul\Desktop\sdl\ConfigurationofThreats.txt");*/

            // To use the domain-specific API:
            //  Create another project with the same name as the paradigm name
            //  Copy the paradigm .mga file to the directory containing the new project
            //  In the new project, install the GME DSMLGenerator NuGet package (search for DSMLGenerator)
            //  Add a Reference in this project to the other project
            //  Add "using [ParadigmName] = ISIS.GME.Dsml.[ParadigmName].Interfaces;" to the top of this file
            // if (currentobj != null && currentobj.Meta.Name == "KindName")
            // [ParadigmName].[KindName] dsCurrentObj = ISIS.GME.Dsml.[ParadigmName].Classes.[KindName].Cast(currentobj);			
        }


        #region IMgaComponentEx Members

        MgaGateway MgaGateway { get; set; }
        GMEConsole GMEConsole { get; set; }

        public void InvokeEx(MgaProject project, MgaFCO currentobj, MgaFCOs selectedobjs, int param)
        {
            if (!enabled)
            {
                return;
            }

            try
            {
                GMEConsole = GMEConsole.CreateFromProject(project);
                MgaGateway = new MgaGateway(project);
                project.CreateTerritoryWithoutSink(out MgaGateway.territory);

                MgaGateway.PerformInTransaction(delegate
                {
                    Main(project, currentobj, selectedobjs, Convert(param));
                });
            }
            finally
            {
                if (MgaGateway.territory != null)
                {
                    MgaGateway.territory.Destroy();
                }
                MgaGateway = null;
                project = null;
                currentobj = null;
                selectedobjs = null;
                GMEConsole = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private ComponentStartMode Convert(int param)
        {
            switch (param)
            {
                case (int)ComponentStartMode.GME_BGCONTEXT_START:
                    return ComponentStartMode.GME_BGCONTEXT_START;
                case (int)ComponentStartMode.GME_BROWSER_START:
                    return ComponentStartMode.GME_BROWSER_START;

                case (int)ComponentStartMode.GME_CONTEXT_START:
                    return ComponentStartMode.GME_CONTEXT_START;

                case (int)ComponentStartMode.GME_EMBEDDED_START:
                    return ComponentStartMode.GME_EMBEDDED_START;

                case (int)ComponentStartMode.GME_ICON_START:
                    return ComponentStartMode.GME_ICON_START;

                case (int)ComponentStartMode.GME_MAIN_START:
                    return ComponentStartMode.GME_MAIN_START;

                case (int)ComponentStartMode.GME_MENU_START:
                    return ComponentStartMode.GME_MENU_START;
                case (int)ComponentStartMode.GME_SILENT_MODE:
                    return ComponentStartMode.GME_SILENT_MODE;
            }

            return ComponentStartMode.GME_SILENT_MODE;
        }

        #region Component Information
        public string ComponentName
        {
            get { return GetType().Name; }
        }

        public string ComponentProgID
        {
            get
            {
                return ComponentConfig.progID;
            }
        }

        public componenttype_enum ComponentType
        {
            get { return ComponentConfig.componentType; }
        }
        public string Paradigm
        {
            get { return ComponentConfig.paradigmName; }
        }
        #endregion

        #region Enabling
        bool enabled = true;
        public void Enable(bool newval)
        {
            enabled = newval;
        }
        #endregion

        #region Interactive Mode
        protected bool interactiveMode = true;
        public bool InteractiveMode
        {
            get
            {
                return interactiveMode;
            }
            set
            {
                interactiveMode = value;
            }
        }
        #endregion

        #region Custom Parameters
        SortedDictionary<string, object> componentParameters = null;

        public object get_ComponentParameter(string Name)
        {
            if (Name == "type")
                return "csharp";

            if (Name == "path")
                return GetType().Assembly.Location;

            if (Name == "fullname")
                return GetType().FullName;

            object value;
            if (componentParameters != null && componentParameters.TryGetValue(Name, out value))
            {
                return value;
            }

            return null;
        }

        public void set_ComponentParameter(string Name, object pVal)
        {
            if (componentParameters == null)
            {
                componentParameters = new SortedDictionary<string, object>();
            }

            componentParameters[Name] = pVal;
        }
        #endregion

        #region Unused Methods
        // Old interface, it is never called for MgaComponentEx interfaces
        public void Invoke(MgaProject Project, MgaFCOs selectedobjs, int param)
        {
            throw new NotImplementedException();
        }

        // Not used by GME
        public void ObjectsInvokeEx(MgaProject Project, MgaObject currentobj, MgaObjects selectedobjs, int param)
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion

        #region IMgaVersionInfo Members

        public GMEInterfaceVersion_enum version
        {
            get { return GMEInterfaceVersion_enum.GMEInterfaceVersion_Current; }
        }

        #endregion

        #region Registration Helpers

        [ComRegisterFunctionAttribute]
        public static void GMERegister(Type t)
        {
            Registrar.RegisterComponentsInGMERegistry();

        }

        [ComUnregisterFunctionAttribute]
        public static void GMEUnRegister(Type t)
        {
            Registrar.UnregisterComponentsInGMERegistry();
        }

        #endregion


    }
}
